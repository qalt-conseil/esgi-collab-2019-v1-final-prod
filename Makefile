.PHONY: install start stop beautify lint tests

install:
	docker-compose run --rm composer install

start:
	docker-compose up -d

stop:
	docker-compose down

beautify:
	docker-compose run --rm php vendor/bin/phpcbf --standard=PSR2 src templates tests

lint:
	docker-compose run --rm php vendor/bin/phpcs --standard=PSR2 src templates tests

tests:
	docker-compose run --rm composer ./bin/phpunit