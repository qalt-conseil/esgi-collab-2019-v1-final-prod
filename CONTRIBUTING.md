# Contributions

## Requirements

- [Git][git]
- [GNU/Make][gnumake]
- [Docker][docker]
- [Docker Compose][dockercompose]

[docker]: https://www.docker.com
[dockercompose]: https://docs.docker.com/compose/
[git]: https://git-scm.com/
[gnumake]: https://www.gnu.org/software/make/

## Choose an issue

Go to https://gitlab.com/qalt-conseil/esgi-collab-2019-v1-final-prod/issues and pick something that you want to work on.

## Fork the project

See the [documentation](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork).

## Clone the repository

### HTTPS

```console
$ git clone https://gitlab.com/$USER/esgi-collab-2019-v1-final-prod.git ~/$USER/esgi-collab-2019-v1-final-prod
```

Where `$USER` is your GitLab's username.

### SSH

```console
$ git clone git@gitlab.com:$USER/esgi-collab-2019-v1-final-prod.git ~/$USER/esgi-collab-2019-v1-final-prod
```

Where `$USER` is your GitLab's username.

## Move to the project's directory

```console
$ cd ~/$USER/esgi-collab-2019-v1-final-prod
```

Where `$USER` is your GitLab's username.

## Create a branch

```console
$ git branch branch-name
```

Where `branch-name` is the name of the branch. Try to choose something that is relevant to the issue.

### branch name naming

|Type|Branch|
|---|---|
|Feature|`feature/#1-name-of-the-feature`|
|Fix|`fix/#1-name-of-the-fix`|
|Refactor|`refactor/name-of-the-refactor`|

The **"#1"** must be the same number for the issue in GitLab, and the **"name-of-the-feature"** same as the GitLab issue's names


## Checkout to the newly created branch

```console
$ git checkout branch-name
```

Where `branch-name` is the name of the branch you just created.

## Install the dependencies

```console
$ make install
```

## Start the containers

```console
$ make start
```

*Note: The website will be available at http://localhost/.*

## Resolve the issue

Append changes until you manage to solve the issue.

## Beautify the project's source-files

```console
$ make beautify
```

## Lint the project's source-files

```console
$ make lint
```

## Test the project

```console
$ make test
```

## Stop the containers

```console
$ make stop
```

## Commit your changes

Try to use commit messages that are relevant to the changes applied to the updated files.

## Push & publish your branch

```console
$ git push --set-upstream origin branch-name
```

Where `branch-name` is the name of the branch you just created.

## Open a pull request

See the [documentation](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
