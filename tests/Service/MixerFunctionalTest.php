<?php

namespace App\Tests\Service;

use Symfony\Component\HttpClient\HttpClient;

class MixerFunctionalTest
{
    public function testApiMixer()
    {
        $client = HttpClient::create();
        $response = $client->request(
            'GET',
            'https://mixer.com/api/v1/channels?order=viewersCurrent:DESC&limit=20&where=languageId:eq:fr'
        );

        $this->assertEquals(200, $response->getStatusCode());
    }
}
