<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CalculatorTest extends WebTestCase
{
    public function testAddition()
    {
        $this->assertEquals(3, CalculatorService::addition(1, 2));
    }

    public function testSubtraction()
    {
        $this->assertEquals(-1, CalculatorService::subtraction(1, 2));
    }

    public function testMultiplication()
    {
        $this->assertEquals(2, CalculatorService::multiplication(1, 2));
    }

    public function testDivision()
    {
        $this->assertEquals(0.5, CalculatorService::division(1, 2));
    }
}
