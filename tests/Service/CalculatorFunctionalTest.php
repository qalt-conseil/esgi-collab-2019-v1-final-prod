<?php

declare(strict_types=1);

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CalculatorFunctionalTest extends WebTestCase
{
    public function testApiAddition()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/addition/1/2');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertJsonStringEqualsJsonString($client->getResponse()->getContent(), json_encode([
            'a' => 1,
            'operator' => '+',
            'b' => 2,
            'result' => 3
        ]));
    }

    public function testApiSubtraction()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/subtraction/1/2');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertJsonStringEqualsJsonString($client->getResponse()->getContent(), json_encode([
            'a' => 1,
            'operator' => '-',
            'b' => 2,
            'result' => -1
        ]));
    }

    public function testApiMultiplication()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/multiplication/1/2');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertJsonStringEqualsJsonString($client->getResponse()->getContent(), json_encode([
            'a' => 1,
            'operator' => '*',
            'b' => 2,
            'result' => 2
        ]));
    }

    public function testApiDivision()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/division/1/2');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertJsonStringEqualsJsonString($client->getResponse()->getContent(), json_encode([
            'a' => 1,
            'operator' => '/',
            'b' => 2,
            'result' => 0.5
        ]));

        $client = static::createClient();

        $client->request('GET', '/api/v1/division/1/0');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}
