[![pipeline status](https://gitlab.com/qalt-conseil/esgi-collab-2019-v1-final-prod/badges/master/pipeline.svg)](https://gitlab.com/qalt-conseil/esgi-collab-2019-v1-final-prod/commits/master)

# QALT Conseil :fire:

The project calls the Mixer API in order to fetch and display all the viewers and followers of each games broadcasted.

## Requirements :package:

- GIT
- Make
- Docker
- Docker-compose

## Installation :wrench:

With GIT SSH: :globe_with_meridians:

```console
$ git clone git@gitlab.com:qalt-conseil/esgi-collab-2019-v1-final-prod.git
$ cd esgi-collab-2019-v1-final-prod
$ make install
$ make start
```

With GIT HTTP: :globe_with_meridians:

```console
$ git clone https://gitlab.com/qalt-conseil/esgi-collab-2019-v1-final-prod.git
$ cd esgi-collab-2019-v1-final-prod
$ make install
$ make start
```

## Usefull commands :rocket:

command | description
---|---
`make start` | Start all container services 
`make install` | Install composer dependencies
`make stop` | Stop all container services 
`make lint` | Check codestyle
`make beautify` | Apply PSR2 coding standard
`make tests` | Run all the tests

## Set GIT Hooks

```console
$ vim .git/hooks/pre-push
```
```bash 
#!/bin/sh

make lint &&
make tests
```

## License 

[MIT](./LICENSE)