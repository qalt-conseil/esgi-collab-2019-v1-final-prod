<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Config\Definition\Exception\Exception;

class CalculatorService
{
    public static function addition(int $a, int $b): int
    {
        return $a + $b;
    }

    public static function subtraction(int $a, int $b): int
    {
        return $a - $b;
    }

    public static function multiplication(int $a, int $b): int
    {
        return $a * $b;
    }

    public static function division(int $a, int $b): float
    {
        return (float) $a / $b;
    }
}
