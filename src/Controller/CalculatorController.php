<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/api/v1/addition/{a}/{b}", name="addition", methods={"GET"})
     */
    public function addition(int $a, int $b)
    {
        $result = CalculatorService::addition($a, $b);

        return $this->json(['a' => $a, 'operator' => '+', 'b' => $b, 'result' => $result]);
    }

    /**
     * @Route("/api/v1/subtraction/{a}/{b}", name="subtraction", methods={"GET"})
     */
    public function subtraction(int $a, int $b)
    {
        $result = CalculatorService::subtraction($a, $b);

        return $this->json(['a' => $a, 'operator' => '-', 'b' => $b, 'result' => $result]);
    }

    /**
     * @Route("/api/v1/multiplication/{a}/{b}", name="multiplication", methods={"GET"})
     */
    public function multiplication(int $a, int $b)
    {
        $result = CalculatorService::multiplication($a, $b);

        return $this->json(['a' => $a, 'operator' => '*', 'b' => $b, 'result' => $result]);
    }

    /**
     * @Route("/api/v1/division/{a}/{b}", name="division", methods={"GET"})
     */
    public function division(int $a, int $b)
    {
        if ($b === 0) {
            return $this->json(null, 400);
        }

        $result = CalculatorService::division($a, $b);

        return $this->json(['a' => $a, 'operator' => '/', 'b' => $b, 'result' => $result]);
    }
}
